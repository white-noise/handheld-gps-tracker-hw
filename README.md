## Handheld GPS Tracker
The Handheld GPS Tracker board tracks our exact location using a GPS breakout board (NEO-7M module), receives the rockets coordinates and finally guides us on the LCD screen making the recovery process way easier. The board also has two switching regulator circuits providing 3.3V and 5V to our breakouts. <br>
!Note that the PCB was designed for the GPS breakout to be placed face down (the GPS module on the breakout should face our tracker).



## Libraries 
In order to use this project one should download LSF's KiCad library (https://gitlab.com/librespacefoundation/lsf-kicad-lib.git) 
and configure LSF_KICAD_LIB as an environment variable in KiCad (Preferences > Configure Paths).



